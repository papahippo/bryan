#!/usr/bin/python3
## #!/usr/bin/sudo /usr/bin/python3
"""
morse code detection using an 'orange pi' (other fruits are available!).
"""
import sys, os, time, subprocess
import orangepi.one
from OPi import GPIO
from morse import Morse

#DING_DAT_DONG = ["/usr/bin/ogg123", "-d", "alsa",
#                  "/usr/share/sounds/KDE-Im-Phone-Ring.ogg"]
DING_DAT_DONG = ["/usr/bin/ssh", "gill@luna", "/usr/bin/ogg123","-d", "alsa",
                 "/usr/share/sounds/KDE-Im-Phone-Ring.ogg"]


class GPIO_Morse(Morse):
    # In het geval van de Orange Pi is 'key_to_poll' een echt pin nummer op de connector.
    #
    key_to_poll = 18

    def __init__(self):
        # Het merendeel van de initialisatie is hetzelfde als bij de prototype
        # morse (met 'alt' toets van normaal toetsenbord) implementatie
        #
        Morse.__init__(self)
        # Configureer de input port als zodanig!
        #
        GPIO.setup(self.key_to_poll, GPIO.IN)

    def poll(self):
        """
        toets de toets! 1/0 => wel/niet ingedrukt
        """
        return GPIO.input(self.key_to_poll)


# Er zit een optocoupler tussen output 'deur' en de sensor ingang van de echte deur.
# Dit zorg ervoor dat er geen gezamenlijke aard nodig is tussen die twee.
#
deur = 16

def pulse_door():
    broken_in = False
    print("door pulse start")
    GPIO.output(deur, 1)
    try:
        time.sleep(0.3)
    except KeyboardInterrupt:
        broken_in = True
    print("door pulse end")
    GPIO.output(deur, 0)
    if broken_in:
        ring_bell()
        sys.exit(0)

def ring_bell():
    print("start ringing bell...")
    p = subprocess.Popen(DING_DAT_DONG)
    p.wait()
    print("...all done ringing bell!")

def main():
    print("Hallo! We gaan spelen met Morse code.")
    print("Gebruik de deurbell om Morse codes in te toetsen.")
    print("Hier is een lijst van alle geldige Morse codes:")

    # Het schijnt dat onze gekozen pinnen (16 en 18) al een functie toegediend hebben voordat wij
    # draaien. Hier moet ik 'een keer' achteraan ggan. Voor nu:
    #
    GPIO.setwarnings(False)

    # Gebruik connector pen nummers van de Orange Pi, niet functionele nummers. Dit is handig in de
    # context van de "Orange Pi One". Aandacht nodig als we overstappen naar een ander Orange Pi model!
    #
    GPIO.setmode(orangepi.one.BOARD)
    GPIO.setup(deur, GPIO.OUT)

    # Hou de output laag, behalve wanneer we een pulse willen geven. Let op! Dit is mischien  problematisch bij
    # een stroomstoring; beter een inverter tussen OPi en de optocoupler gebruiken?
    #
    GPIO.output(deur, 0)

    morse = GPIO_Morse()
    morse.print_table()

    while True:
        try:
            mijn_char = morse.get_char()
        except KeyboardInterrupt:
            pulse_door()
            continue
        except Morse.Error as me:
            print(f"morse code fout!: {me}")
            continue
        except Morse.Break as mb:
            print(f"{mb}:\n ... behandel lange druk op be bel echt als deurbel!")
            ring_bell()
            continue
        if mijn_char is None:
            continue

        print(f' = {mijn_char}')
        if mijn_char == 'T':
            morse.print_table()
        elif mijn_char == 'B':
            print (f"Hello Bryan!")
        elif mijn_char == 'Q':
            print (f"Bye Brye!")
            return
        elif mijn_char == 'D':
            pulse_door()

if __name__ == "__main__":
    main()
